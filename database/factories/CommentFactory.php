<?php
require_once 'vendor/autoload.php';

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Comment;
use App\Post;
use App\User;
use Illuminate\Support\Str;
// use Faker\Generator as Faker;

$factory->define(Comment::class, function () {
    $faker = Faker\Factory::create();
    return [
        'user_id' => User::get('id')->random()->id,
        'post_id' => Post::get('post_id')->random()->post_id,
        'comment' => $faker->sentence($nbWords = 6, $variableNbWords = true),
    ];
});
