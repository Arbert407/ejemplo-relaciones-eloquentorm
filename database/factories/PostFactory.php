<?php
require_once 'vendor/autoload.php';
/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Post;
use App\User;
use Illuminate\Support\Str;

// use Faker\Generator as Faker;

$factory->define(Post::class, function () {
    $faker = Faker\Factory::create();
    return [
        'user_id' =>  User::get('id')->random()->id,
        'post' => $faker->text
    ];
});
