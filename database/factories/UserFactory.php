<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\User;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    $image = [
        'https://b.thumbs.redditmedia.com/bklLsi-PwlfkOjcHguSxnT-AQ_6McHbSzquxax0guAM.png',
        'https://pbs.twimg.com/profile_images/699903473144320000/ZN4Fx1in_400x400.jpg',
        'https://avatarfiles.alphacoders.com/159/159325.jpg',
        'https://prod-profile.pscp.tv/1JREmROqDVLQP/8125b91363967b3cbd5665d664f6aa93-256.jpg',
        'https://steamuserimages-a.akamaihd.net/ugc/961971654931781317/B8CF9F6ED4DD4582B2B646EE2AB3DA64F79A43C3/?imw=637&imh=358&ima=fit&impolicy=Letterbox&imcolor=%23000000&letterbox=true',
        'https://cdn140.picsart.com/304058277121201.jpg?type=webp&to=crop&r=256',
        'https://lh3.googleusercontent.com/-h27rC_ESNrs/AAAAAAAAAAI/AAAAAAAAAAA/AKF05nBeWCzSxJDfHP0nwqNh0Y10BgLl6g/photo.jpg',
        'https://i.pinimg.com/originals/e9/bd/99/e9bd99ee6d73b823a6ede2c972b920a9.png',
        'https://i.pinimg.com/280x280_RS/2f/1a/e8/2f1ae8093621b1c1432f4ee9c22d607a.jpg',
        'https://i.pinimg.com/280x280_RS/2f/1a/e8/2f1ae8093621b1c1432f4ee9c22d607a.jpg',
        'https://lh3.googleusercontent.com/proxy/mIz_wo1dHjaNjlVXEbmJ49gQ-uLPMOUB69dFK6WVDH4FzBxvH9mAH7su0pKT1JodvoszEr3kvmFQrypnVtv-a0cpFUYFAgdy',
        'https://img.taplb.com/md5/3c/89/3c89ea9f5351b47258f85d5d9984de9c?TapTapIcon/size/256x256',
        'https://cdn141.picsart.com/322317404506201.jpg?type=webp&to=crop&r=256',
    ];

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'email_verified_at' => now(),
        'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
        'remember_token' => Str::random(10),
        'image' => $image[array_rand($image)]

    ];
});
