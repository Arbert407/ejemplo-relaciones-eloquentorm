<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Phone;
class PhoneSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = User::all();
        for($i = 0;$i < count($users) ;$i++ ){
            factory(Phone::class)->create([
                'user_id' => $users[$i]->id,
            ]);
        }
    }
}
