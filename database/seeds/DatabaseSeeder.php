<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */


    public function run()
    {  
      
      $this->truncateTables([
          'roles',
          'users',
          'role_user',
          'posts',
          'comments',
          'phones',
          'role_user'
      ]);

      $this->call(UserSeeder::class);
      $this->call(RoleSeeder::class);
      $this->call(PhoneSeeder::class);
      $this->call(PostSeeder::class);
      $this->call(CommentSeeder::class);
      $this->call(RoleUserSeeder::class);
    }


    protected function truncateTables(array $tables){
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        foreach ($tables as $table) {
            DB::table($table)->truncate();
        }
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
