<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;
use App\RoleUser;


class RoleUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (User::get('id') as $id) {
            RoleUser::create([
                'user_id' => $id->id,
                'role_id' => Role::get('id')->random()->id
            ]);
        }
    }
}
