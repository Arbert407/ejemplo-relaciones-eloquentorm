<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    protected $fillable = [
        'name'
    ];

    public function phone(){
        return $this->hasOne('App\Phone');
    }

    public function roles()
    {
        return $this->belongsToMany('App\Role','role_user')
                    ->as('role_user')
                    ->using('App\RoleUser')
                    ->withTimestamps();
        
    }

    public function posts()
    {
        return $this->hasMany('App\Post', 'user_id', 'id');
    }

    public function comments()
    {
        return $this->hasMany('App\Comment', 'user_id', 'id');
    }
}





